.pc = $c000

  sei

  lda #0
  sta $d020

// All pins on port b to output mode
  lda #0
  sta $dd03

// Point NMI interrupt vector to nmiroutine
  lda #<nmiroutine
  sta $318
  lda #>nmiroutine
  sta $319

// Only FLAG line on user port B causes an interrupt
  lda #$90
  sta $dd0d

  lda #0
  sta $fb
  lda #4
  sta $fc

loop:
  lda exec
  beq norun
  jmp run
norun:
  jmp loop

nmiroutine:
  pha
  lda $dd01
  jsr decode
  lda $dd0d
  pla
  rti

.pc = $c0c0
exec:
.byte 0
run:
  jsr $a659
  jmp $a7ae

temp:
.byte 0
hex:
.text "0123456789abcdef"
// decode byte in accumulator
decode:
  tay
  ldx state
  lda statelo,x
  sta statepointer
  lda statehi,x
  sta statepointer+1
  tya
  jmp (statepointer)

normal:
  tay
  cmp #4
  bne notescape
  lda #1
  sta state
  rts
notescape:
  tya
  cmp #13
  bne notaddr
  lda #3
  sta state
  rts
notaddr:
  ldy #0
  sta ($fb),y
  clc
  lda $fb
  adc #1
  sta $fb
  lda $fc
  adc #0
  sta $fc
  rts

escaped:
  ldy #0
  sta ($fb),y
  lda #0
  sta state
  clc
  lda $fb
  adc #1
  sta $fb
  lda $fc
  adc #0
  sta $fc
  rts

addrhi:
  sta $fc
  lda #0
  sta state
  rts

addrlo:
  sta $fb
  lda #2
  sta state
  rts

state:
.byte 0

statepointer:
.byte 0, 0

//value 4 is an escape, value 13 is an address set cmd
//States:
//0 Normal read
//1 Escaped, don't check for address set, goto 0
//2 Address write hi goto 0
//3 Address write lo, goto 2

statelo:
.byte <normal
.byte <escaped
.byte <addrhi
.byte <addrlo

statehi:
.byte >normal
.byte >escaped
.byte >addrhi
.byte >addrlo

