#! /usr/bin/env python

import serial
import time
import random

port = '/dev/tty.usbmodem1411'

ser = serial.Serial(port, 9600)
ser.close()
ser = serial.Serial(port, 57600)

time.sleep(4)

s = ''.join([chr(x & 0xff) for x in range(1000)])

ser.write('\x0d\x00\x04')

for c in s:
    c = random.choice([chr(205), chr(206)])
    if c in '\x04\x0d':
        ser.write('\x04')
    ser.write(c)

ser.flush()
ser.close()
print "Done!"

