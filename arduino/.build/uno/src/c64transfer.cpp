#include <Arduino.h>
void setup();
void loop();
#line 1 "src/c64transfer.ino"
int dataPins[8] = {10, 9, 8, 7, 6, 5, 4, 3};

int resetPin = 11;
int flagPin = 12;
int flag = 1;
unsigned char lastByte;
unsigned char i = 0;
int a = 0;
void setup() {
  for(i; i < 8; i++){
    pinMode(dataPins[i], OUTPUT);
  }
  pinMode(flagPin, OUTPUT);
  digitalWrite(flagPin, HIGH);
  pinMode(resetPin, OUTPUT);
  digitalWrite(resetPin, LOW);
  digitalWrite(resetPin, HIGH);
  Serial.end();
  Serial.begin(57600);
}

void loop() {
  if(Serial.available() > 0) {
    lastByte = Serial.read();
    i=8;
    while(i--){
      digitalWrite(dataPins[7-i], lastByte & 1);
      lastByte >>= 1;
    }
    digitalWrite(flagPin, 0);
    digitalWrite(flagPin, 1);
  }
}
