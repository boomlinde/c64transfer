//ROM header stuff

.pc=$8000

.byte	$09, $80 // Cartridge cold-start vector = $8009
.byte	$25, $80 // Cartridge warm-start vector = $8025
.byte	$c3, $c2, $cd, $38, $30 // CBM8O - Autostart key



//KERNAL RESET ROUTINE
  stx $d016 // Turn on VIC for PAL / NTSC check
  jsr $fda3 // IOINIT - Init CIA chips
  jsr $fd50 // RANTAM - Clear/test system RAM
  jsr $fd15 // RESTOR - Init KERNAL RAM vectors
  jsr $ff5b // CINT   - Init VIC and screen editor
  cli // Re-enable IRQ interrupts
//BASIC RESET  Routine
  jsr $e453 // Init BASIC RAM vectors
  jsr $e3bf // Main BASIC RAM Init routine
  jsr $e422 // Power-up message / NEW command
  ldx #$fb
  txs // Reduce stack pointer for BASIC

/////////////////////////////////////////////////////////////////////////////////
  lda #$00
  sta $d020
  sta $d021
  ldx #0

// Copy program to RAM
loop:
  lda code,x
  sta $c000,x
  lda code+$100,x
  sta $c100,x
  lda code+$200,x
  sta $c200,x
  lda code+$300,x
  sta $c300,x
  lda code+$400,x
  sta $c400,x
  inc $d020
  inx
  bne loop

  lda #0
  sta $d020
  jmp $c000

code:  
  .import c64 "transfer_code.prg"

.pc=$a000
